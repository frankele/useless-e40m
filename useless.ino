//constants
const byte DPDT = 6;
const byte LIMIT = 8;
const byte DRIVER_1 = 3;
const byte DRIVER_2 = 19;//A5
const byte STATE_WAIT = 0;
const byte STATE_FORWARD = 1;
const byte STATE_REVERSE = 2;
const byte TRICK_HOVER = 0;
const byte TRICK_RANDOM = 1;
const byte TRICK_FAKE_NORMAL = 2;
const byte TRICK_UNSET = 255;
const int  NUM_TRICKS = 3;
const int  MIN_TIME_TO_SWITCH = 375;//minimum milliseconds for finger to hit switch in 10 trials
//globals
int dpdtVal, limitVal;
byte currentState, currentTrick;//start out with no trick initially
bool flag;//used by different tricks to keep track of completed actions
int peeksLeft;//used by trick fake_normal to count times it has faked
void setup() {
  //init globals -- dpdtVal and limitVal will be set immediately when entering loop
  currentState = 0;
  currentTrick = TRICK_UNSET;
  flag = false;
  peeksLeft = -1;
  //inputs
  pinMode(DPDT, INPUT_PULLUP);
  pinMode(LIMIT, INPUT_PULLUP);
  //outputs
  pinMode(DRIVER_1, OUTPUT);
  pinMode(DRIVER_2, OUTPUT);
  //serial setup
  Serial.begin(115200);
  //seed PRNG with disconnected analog pin
  randomSeed(analogRead(2));
}

void loop() {
  readSwitches();
  pickTrickIfUnset();
  updateState();
  handleState();
}

//updates value of dpdtVal and limitVal
void readSwitches() {
  dpdtVal = digitalRead(DPDT);
  limitVal = digitalRead(LIMIT);
}

void pickTrickIfUnset() {
  if (dpdtVal == HIGH && currentTrick == TRICK_UNSET) {
    currentTrick = (byte) random(0, NUM_TRICKS);//tricks indexed from 0
    currentState = STATE_WAIT;//just to be safe
  }
}

void updateState() {
  switch (currentTrick) {//default case randomizes current trick
    case TRICK_HOVER:
      handleHoverTrick();
      break;
    case TRICK_RANDOM:
      handleRandomWaitTrick();
      break;
    case TRICK_FAKE_NORMAL:
      handleFakeNormalTrick();
      break;
    default:
      break;
  }
}

//real state machine logic
//writes appropriate outputs to control motor
void handleState() {
  switch (currentState) {
    case STATE_FORWARD:
      digitalWrite(DRIVER_1, HIGH);
      digitalWrite(DRIVER_2, LOW);
      break;
    case STATE_REVERSE:
      digitalWrite(DRIVER_1, LOW);
      digitalWrite(DRIVER_2, HIGH);
      break;
    default:
      digitalWrite(DRIVER_1, LOW);
      digitalWrite(DRIVER_2, LOW);
  }
}

//behavior for basic useless box
void defaultStateLogic() {
  if (dpdtVal == LOW && limitVal == LOW) {
    currentState = STATE_REVERSE;
  } else if (dpdtVal == HIGH) {//doesn't matter which pos limit is in, should go forward regardless
    currentState = STATE_FORWARD;
  } else {//default to wait
    currentState = STATE_WAIT;
  }
}

//----------TRICK HANDLERS---------
void handleHoverTrick() {
  if (currentState == STATE_WAIT && limitVal == HIGH && dpdtVal == HIGH) {//fully retracted and switch forward
    currentState = STATE_FORWARD;
  } else if (currentState == STATE_FORWARD && dpdtVal == HIGH && !flag) {//if it is going forward, delay then stop
    delay(MIN_TIME_TO_SWITCH - 100);
    flag = true;
    currentState = STATE_WAIT;
  } else if (currentState == STATE_WAIT && dpdtVal == HIGH) {//hovering
    delay(random(500, 5001));
    currentState = STATE_FORWARD;
  } else if (dpdtVal == LOW && limitVal == LOW) {
    currentState = STATE_REVERSE;
  } else if (limitVal == HIGH) {
    currentState = STATE_WAIT;
    currentTrick = TRICK_UNSET;
    flag = false;
  }
}

void handleRandomWaitTrick() {
  if (currentState == STATE_WAIT  && !flag) {
    delay(random(100, 2500));
    currentState = STATE_FORWARD;
    flag = true;
  } else if (currentState == STATE_WAIT) { //don't need the and here bc if/else structure
    //returned to retracted
    currentTrick = TRICK_UNSET;
    flag = false;
    currentState = STATE_WAIT;
  } else {
    defaultStateLogic();
  }
}

void handleFakeNormalTrick() {
  if (peeksLeft < 0) {
    peeksLeft = random(1, 11);
  }
  if (dpdtVal == HIGH) {
    currentState = STATE_FORWARD;
  } else if (dpdtVal == LOW && limitVal == LOW && currentState != STATE_WAIT) { //"reverse" and not waiting
    if (flag && peeksLeft > 0) {//reversed and should still peek
      delay(100);
      currentState = STATE_WAIT;
      peeksLeft--;
      flag = false;
    } else if (peeksLeft <= 0) {//go down when done
      currentState = STATE_REVERSE;
    } else {
      flag = true;
      currentState = STATE_REVERSE;
    }
  } else if (limitVal == HIGH) {//stop if we hit that limit
    currentTrick = TRICK_UNSET;
    currentState = STATE_WAIT;
    flag = false;
    peeksLeft = -1;
  } else {
    currentState = STATE_WAIT;
  }
}
